<footer class="section-footer bg-inverse" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-5">
                <div class="media">
                    <div class="media-left">
                        <span class="media-object icon-logo display-1"></span>
                    </div>
                    <small class="media-body media-bottom">
                        &copy; لند دات ای او 2015 <br>
                        طراحی شده توسط پیتر فینلان ، کد نویسی توسط تاتی گراسینی ، فارسی شده توسط نودتم
                    </small>
                </div>
            </div>
            <div class="col-md-6 col-lg-7">
                <ul class="list-inline m-b-0">
                    <li class="active"><a href="http://www.20script.ir">دانلود قالب </a></li>
                    <li><a href="#">UI Kit</a></li>
                    <li><a href="https://github.com/tatygrassini/landio-html" target="_blank">گیت هاب</a></li>
                    <li><a class="scroll-top" href="#totop">بازگشت به بالا <span class="icon-caret-up"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() . '/js/landio.min.js'; ?>"></script>

<?php wp_footer(); ?>

</body>
</html>
