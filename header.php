<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta charset="<?php bloginfo('charset'); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php wp_title(' | ', 'true', 'right'); ?><?php bloginfo('name'); ?></title>
    <link href="<?php echo get_bloginfo('rss2_url'); ?>" title="<?php bloginfo('name'); ?>" type="application/rss+xml"
          rel="alternate">


    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/animate.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/landio.css'; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/fa.css'; ?>">

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

    <?php wp_head(); ?>

</head>

<body>

<nav

    <?php if (is_front_page() || is_home()) {
        echo 'class="navbar navbar-dark bg-inverse bg-inverse-custom navbar-fixed-top"';
    } else {
        echo 'class="navbar navbar-dark bg-inverse mb-80 p-relative"';
    } ?>
>
    <div class="container">
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <span class="icon-logo"></span>
            <span class="sr-only">لند دات ای او</span>
        </a>
        <a class="navbar-toggler hidden-md-up pull-right" data-toggle="collapse" href="#collapsingNavbar"
           aria-expanded="false" aria-controls="collapsingNavbar">
            &#9776;
        </a>
        <a class="navbar-toggler navbar-toggler-custom hidden-md-up pull-right" data-toggle="collapse"
           href="#collapsingMobileUser" aria-expanded="false" aria-controls="collapsingMobileUser">
            <span class="icon-user"></span>
        </a>
        <div id="collapsingNavbar" class="collapse navbar-toggleable-custom" role="tabpanel"
             aria-labelledby="collapsingNavbar">
            <ul class="nav navbar-nav pull-right">
                <li class="nav-item nav-item-toggable active">
                    <a class="nav-link" href="http://www.20script.ir">درباره ی لند دات ای او<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item nav-item-toggable">
                    <a class="nav-link" href="#">UI Kit</a>
                </li>
                <li class="nav-item nav-item-toggable">
                    <a class="nav-link" href="https://github.com/tatygrassini/landio-html" target="_blank">گیت هاب</a>
                </li>
                <li class="nav-item nav-item-toggable hidden-sm-up">
                    <form class="navbar-form">
                        <input class="form-control navbar-search-input" type="text"
                               placeholder="کلمه ی مورد نظر خود را جستوجو کنید">
                    </form>
                </li>
                <li class="navbar-divider hidden-sm-down"></li>
                <li class="nav-item dropdown nav-dropdown-search hidden-sm-down">
                    <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="icon-search"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-search" aria-labelledby="dropdownMenu1">
                        <form class="navbar-form">
                            <input class="form-control navbar-search-input" type="text"
                                   placeholder="کلمه ی مورد نظر خود را جستوجو کنید">
                        </form>
                    </div>
                </li>
                <li class="nav-item dropdown hidden-sm-down textselect-off">
                    <a class="nav-link dropdown-toggle nav-dropdown-user" id="dropdownMenu2" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <span class="icon-caret-down"></span> <img
                                src="<?php echo get_template_directory_uri() . '/img/face3.jpg'; ?>" height="40"
                                width="40"
                                alt="Avatar" class="img-circle">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-user dropdown-menu-animated"
                         aria-labelledby="dropdownMenu2">
                        <div class="media">
                            <div class="media-left">
                                <img src="<?php echo get_template_directory_uri() . '/img/face5.jpg'; ?>" height="60"
                                     width="60" alt="Avatar" class="img-circle">
                            </div>
                            <div class="media-body media-middle">
                                <h5 class="media-heading">حسن پورقناد</h5>
                                <h6>info@90theme.ir</h6>
                            </div>
                        </div>
                        <a href="#" class="dropdown-item text-uppercase">نمایش پست ها</a>
                        <a href="#" class="dropdown-item text-uppercase">مدیریت گروه</a>
                        <a href="#" class="dropdown-item text-uppercase">پرداخت قبض ها</a>
                        <a href="#" class="dropdown-item text-uppercase text-muted">خروج</a>
                        <a href="#" class="btn-circle has-gradient pull-left">
                            <span class="sr-only">ویرایش</span>
                            <span class="icon-edit"></span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
        <div id="collapsingMobileUser" class="collapse navbar-toggleable-custom dropdown-menu-custom p-x hidden-md-up"
             role="tabpanel" aria-labelledby="collapsingMobileUser">
            <div class="media m-t">
                <div class="media-left">
                    <img src="<?php echo get_template_directory_uri() . '/img/face5.jpg'; ?>" height="60" width="60"
                         alt="Avatar" class="img-circle">
                </div>
                <div class="media-body media-middle">
                    <h5 class="media-heading">حسن پورقناد</h5>
                    <h6>info@90theme.ir</h6>
                </div>
            </div>
            <a href="#" class="dropdown-item text-uppercase">نمایش پست ها</a>
            <a href="#" class="dropdown-item text-uppercase">مدیریت گروه</a>
            <a href="#" class="dropdown-item text-uppercase">پرداخت قبض ها</a>
            <a href="#" class="dropdown-item text-uppercase text-muted">خروج</a>
            <a href="#" class="btn-circle has-gradient pull-left">
                <span class="sr-only">ویرایش</span>
                <span class="icon-edit"></span>
            </a>
        </div>
    </div>
</nav>
