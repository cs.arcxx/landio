<?php get_header(); ?>



    <header class="jumbotron bg-inverse text-center center-vertically" role="banner">
        <div class="container">
            <h1 class="display-3">سعی کنید تولید کننده باشید !!</h1>
            <h2 class="m-b-lg">این استایل به صورت <em>کاملا رایگان</em>توسط <a href="http://90theme.ir" class="jumbolink">نودتم
                    فارسی شده</a>.</h2>
            <a class="btn btn-secondary-outline m-b-md" href="#" role="button"><span class="icon-sketch"></span>دریافت فایل
                گرافیکی</a>
            <ul class="list-inline social-share">
                <li><a class="nav-link" href="#"><span class="icon-twitter"></span> 1024</a></li>
                <li><a class="nav-link" href="#"><span class="icon-facebook"></span> 562</a></li>
                <li><a class="nav-link" href="#"><span class="icon-linkedin"></span> 356</a></li>
            </ul>
        </div>
    </header>

    <section class="section-intro bg-faded text-center hidden-overflow">
        <div class="container">
            <h3 class="wp wp-1">صفحه ی فرود زیبا و کاربرپسند داشته باشید</h3>
            <p class="lead wp wp-2">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                گرافیک
                است</p>
            <img src="<?php echo get_template_directory_uri() . '/img/mock.png'; ?>" alt="iPad mock"
                 class="img-responsive wp wp-3">
        </div>
    </section>
    <section class="section-features text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-block">
                            <span class="icon-pen display-1"></span>
                            <h4 class="card-title">قدرتمند</h4>
                            <h6 class="card-subtitle text-muted">مانند شیر</h6>
                            <p class="card-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                                از
                                طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-block">
                            <span class="icon-thunderbolt display-1"></span>
                            <h4 class="card-title">چشم نواز</h4>
                            <h6 class="card-subtitle text-muted">با طراحی مدرن</h6>
                            <p class="card-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                                از
                                طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card m-b-0">
                        <div class="card-block">
                            <span class="icon-heart display-1"></span>
                            <h4 class="card-title">رایگان</h4>
                            <h6 class="card-subtitle text-muted">برای همیشه </h6>
                            <p class="card-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                                از
                                طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-video bg-inverse text-center wp wp-4">
        <h3 class="sr-only">Video</h3>
        <video id="demo_video" class="video-js vjs-default-skin vjs-big-play-centered" controls
               poster="<?php echo get_template_directory_uri() . '/img/video-poster.jpg'; ?>" data-setup='{}'>
            <source src="http://vjs.zencdn.net/v/oceans.mp4" type='video/mp4'>
            <source src="http://vjs.zencdn.net/v/oceans.webm" type='video/webm'>
        </video>
    </section>
    <section class="section-pricing bg-faded text-center">
        <div class="container">
            <h3>مدیریت هزینه ها</h3>
            <div class="row p-y-lg">
                <div class="col-md-4 p-t-md wp wp-5">
                    <div class="card pricing-box">
                        <div class="card-header text-uppercase">
                            شخصی
                        </div>
                        <div class="card-block">
                            <p class="card-title">برای استفاده ی شخصی یک فرد عادی و نهایت استفاده تا چهار بار در یک
                                ماه</p>
                            <h4 class="card-text">
                                <sup class="pricing-box-currency">هزار تومان</sup>
                                <span class="pricing-box-price">20</span>
                                <small class="text-muted text-uppercase">در ماه</small>
                            </h4>
                        </div>
                        <ul class="list-group list-group-flush p-x">
                            <li class="list-group-item">امکانات بسیار بالا</li>
                            <li class="list-group-item">تمام موارد بیمه</li>
                            <li class="list-group-item">واریز 2 بار در روز</li>
                        </ul>
                        <a href="#" class="btn btn-primary-outline">شروع کنید</a>
                    </div>
                </div>
                <div class="col-md-4 stacking-top">
                    <div class="card pricing-box pricing-best p-x-0">
                        <div class="card-header text-uppercase">
                            حرفه ای
                        </div>
                        <div class="card-block">
                            <p class="card-title">برای استفاده ی افراد حرفه ای و کسانی که شرکت های متوسط یا کسب و کار
                                های
                                پول ساز دارند</p>
                            <h4 class="card-text">
                                <sup class="pricing-box-currency tv-xa-nt">هزار تومان</sup>
                                <span class="pricing-box-price">32</span>
                                <small class="text-muted text-uppercase">در ماه</small>
                            </h4>
                        </div>
                        <ul class="list-group list-group-flush p-x">
                            <li class="list-group-item">امکانات بسیار بالا</li>
                            <li class="list-group-item">تمام موارد بیمه</li>
                            <li class="list-group-item">واریز 5 بار در روز</li>
                            <li class="list-group-item">یک دامنه ی رایگان</li>
                        </ul>
                        <a href="#" class="btn btn-primary">شروع کنید</a>
                    </div>
                </div>
                <div class="col-md-4 p-t-md wp wp-6">
                    <div class="card pricing-box">
                        <div class="card-header text-uppercase">
                            فوق حرفه ای
                        </div>
                        <div class="card-block">
                            <p class="card-title">مناسب شرکت های بزرگ یا بانک ها و سرمایه گزار ها یا اختلاص کننده ها
                                :)</p>
                            <h4 class="card-text">
                                <sup class="pricing-box-currency">هزار تومان</sup>
                                <span class="pricing-box-price">80</span>
                                <small class="text-muted text-uppercase">در ماه</small>
                            </h4>
                        </div>
                        <ul class="list-group list-group-flush p-x">
                            <li class="list-group-item">تمام موارد بیمه</li>
                            <li class="list-group-item">واریز 5 بار در روز</li>
                            <li class="list-group-item">یک دامنه ی رایگان</li>
                        </ul>
                        <a href="#" class="btn btn-primary-outline">شروع کنید</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-testimonials text-center bg-inverse">
        <div class="container">
            <h3 class="sr-only">دیدگاه ها</h3>
            <div id="carousel-testimonials" class="carousel slide" data-ride="carousel" data-interval="5000">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <blockquote class="blockquote">
                            <img src="<?php echo get_template_directory_uri() . '/img/face1.jpg'; ?>" height="80"
                                 width="80" alt="Avatar" class="img-circle">
                            <p class="h3">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                                صنعت
                                چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</p>
                            <footer>امیر عباس خدایی</footer>
                        </blockquote>
                    </div>
                    <div class="carousel-item">
                        <blockquote class="blockquote">
                            <img src="<?php echo get_template_directory_uri() . '/img/face2.jpg'; ?>" height="80"
                                 width="80" alt="Avatar" class="img-circle">
                            <p class="h3">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                                صنعت
                                چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</p>
                            <footer>بچه ها فاز سه</footer>
                        </blockquote>
                    </div>
                    <div class="carousel-item">
                        <blockquote class="blockquote">
                            <img src="<?php echo get_template_directory_uri() . '/img/face3.jpg'; ?>" height="80"
                                 width="80" alt="Avatar" class="img-circle">
                            <p class="h3">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                                صنعت
                                چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</p>
                            <footer>بچه های شهرک</footer>
                        </blockquote>
                    </div>
                    <div class="carousel-item">
                        <blockquote class="blockquote">
                            <img src="<?php echo get_template_directory_uri() . '/img/face4.jpg'; ?>" height="80"
                                 width="80" alt="Avatar" class="img-circle">
                            <p class="h3">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                                صنعت
                                چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</p>
                            <footer>عرفان خفن</footer>
                        </blockquote>
                    </div>
                    <div class="carousel-item">
                        <blockquote class="blockquote">
                            <img src="<?php echo get_template_directory_uri() . '/img/face5.jpg'; ?>" height="80"
                                 width="80" alt="Avatar" class="img-circle">
                            <p class="h3">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                                صنعت
                                چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</p>
                            <footer>احمد نوری</footer>
                        </blockquote>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li class="active"><img src="<?php echo get_template_directory_uri() . '/img/face1.jpg'; ?>"
                                            alt="Navigation avatar" data-target="#carousel-testimonials"
                                            data-slide-to="0" class="img-responsive img-circle"></li>
                    <li><img src="<?php echo get_template_directory_uri() . '/img/face2.jpg'; ?>"
                             alt="Navigation avatar" data-target="#carousel-testimonials"
                             data-slide-to="1" class="img-responsive img-circle"></li>
                    <li><img src="<?php echo get_template_directory_uri() . '/img/face3.jpg'; ?>"
                             alt="Navigation avatar" data-target="#carousel-testimonials"
                             data-slide-to="2" class="img-responsive img-circle"></li>
                    <li><img src="<?php echo get_template_directory_uri() . '/img/face4.jpg'; ?>"
                             alt="Navigation avatar" data-target="#carousel-testimonials"
                             data-slide-to="3" class="img-responsive img-circle"></li>
                    <li><img src="<?php echo get_template_directory_uri() . '/img/face5.jpg'; ?>"
                             alt="Navigation avatar" data-target="#carousel-testimonials"
                             data-slide-to="4" class="img-responsive img-circle"></li>
                </ol>
            </div>
        </div>
    </section>
    <section class="section-text">
        <div class="container">
            <h3 class="text-center">در مورد شرکت ما مطمئن باشید</h3>
            <div class="row p-y-lg">
                <div class="col-md-5">
                    <p class="wp wp-7">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                        صنعت
                        چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی
                        برای
                        پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                        گرافیکی
                        نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از
                        متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه
                        طراحی
                        یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها
                        چگونه در نظر گرفته شده‌است.</p>
                </div>
                <div class="col-md-5 col-md-offset-2 separator-x">
                    <p class="wp wp-8">لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در
                        صنعت
                        چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی
                        برای
                        پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                        گرافیکی
                        نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از
                        متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه
                        طراحی
                        یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها
                        چگونه در نظر گرفته شده‌است.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="section-news">
        <div class="container">
            <h3 class="sr-only">اخبار</h3>
            <div class="bg-inverse">

                <?php
                $slug = (get_queried_object()) ? get_queried_object()->slug : null;
                $args = array(
                    'post_status' => 'publish',
                    'post_type' => 'post',
                    'nopaging' => true,
                );
                if (is_category()) {
                    $args['category_name'] = $slug;
                } elseif (is_tag()) {
                    $args['tag_slug__in'] = $slug;
                }
                $myposts = get_posts($args);

                $the_year = null;
                $the_month = null;
                $first_year = true;
                $first_month = true;
                $count = 0;

                for ($i = 0;
                $i < 2;
                $i++) {

                if (sizeof($myposts) < $i) {
                    return;
                }


                $post = $myposts[$i];
                setup_postdata($post);
                $count++;
                ?>

                <?php
                if (get_option('showing_months_in_archive') && $the_month != get_the_date('M')) {
                    $the_month = get_the_date('M');
                    echo $the_month . '<br>';
                }

                $tag_title = get_post_meta(get_the_ID(), 'meta-tag-title', true);
                $tag_color = get_post_meta(get_the_ID(), 'meta-tag-color', true);

                ?>

                <div class="row">

                    <?php if (($count % 2) == 0) {
                        echo '<div class="col-md-6 p-r-0">';
                    } else {
                        echo '<div class="col-md-6 col-md-push-6 p-l-0">';
                    } ?>


                    <figure class="has-light-mask m-b-0 image-effect">
                        <?php
                        if (has_post_thumbnail()) { ?>

                            <img src="<?php the_post_thumbnail_url('full'); ?>"
                                 alt="Article thumbnail" class="img-responsive">
                            <?php
                        }
                        ?>

                    </figure>
                </div>

                <?php if (($count % 2) == 0) {
                    echo '<div class="col-md-6 p-l-0">';
                } else {
                    echo '<div class="col-md-6 col-md-pull-6 p-r-0">';
                } ?>


                <article class="center-block">
                    <span class="label label-info"><?php the_category(' '); ?></span>
                    <br>
                    <h5>
                        <a href="<?php the_permalink() ?>"><?= the_title(); ?> <span
                                    class="icon-arrow-right pr-30"></span> </a>
                    </h5>
                    <p class="m-b-0">


                        <?php
                        if (get_the_tags()) { ?>

                            <a href="#"><span class="label label-default text-uppercase"><span
                                            class="icon-tag">
                                            </span><?php the_tags(' ', '&nbsp;  ,  &nbsp;', ' '); ?> </span>
                            </a>

                            <?php
                        }
                        ?>

                        <a href="#"><span class="label label-default text-uppercase">
                                            <?= (get_option('showing_months_in_archive')) ? get_the_date('d') : get_the_date('m/d') ?>
                                <span class="icon-time float-right"></span>

                                        </span></a>
                    </p>
                </article>
            </div>

        </div>


        <?php if ($tag_title && get_option('meta_tag_option')) : ?>
            <?php echo $tag_title ?>
        <?php endif; ?>

        <?php if (get_option('showing_comments_count_in_archive')) : ?>
            <span class="archive-comment-count"><?= comments_number(0, 1, '%') ?></span>
        <?php endif; ?>


        <?php

        }

        wp_reset_postdata();
        ?>


        </div>
        </div>
    </section>
<!--    <section class="section-signup bg-faded">-->
<!--        <div class="container">-->
<!--            <h3 class="text-center m-b-lg">ثبت نام کنید و تا آخر عمر از خدمات ما لذت ببرید</h3>-->
<!--            <form>-->
<!--                <div class="row">-->
<!--                    <div class="col-md-6 col-xl-3">-->
<!--                        <div class="form-group has-icon-left form-control-name">-->
<!--                            <label class="sr-only" for="inputName">اسم</label>-->
<!--                            <input type="text" class="form-control form-control-lg" id="inputName" placeholder="اسم">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-6 col-xl-3">-->
<!--                        <div class="form-group has-icon-left form-control-email">-->
<!--                            <label class="sr-only" for="inputEmail">ایمیل</label>-->
<!--                            <input type="email" class="form-control form-control-lg" id="inputEmail" placeholder="ایمیل"-->
<!--                                   autocomplete="off">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-6 col-xl-3">-->
<!--                        <div class="form-group has-icon-left form-control-password">-->
<!--                            <label class="sr-only" for="inputPassword">رمز عبور</label>-->
<!--                            <input type="password" class="form-control form-control-lg" id="inputPassword"-->
<!--                                   placeholder="رمز عبور" autocomplete="off">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-6 col-xl-3">-->
<!--                        <div class="form-group">-->
<!--                            <button type="submit" class="btn btn-primary btn-block">رایگان ثبت نام کنید</button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <label class="c-input c-checkbox">-->
<!--                    <input type="checkbox" checked>-->
<!--                    <span class="c-indicator"></span><a href="http://90theme.ir">قوانین و شرایط</a> این شرکت را می پذیرم-->
<!--                </label>-->
<!--            </form>-->
<!--        </div>-->
<!--    </section>-->
<?php get_footer(); ?>